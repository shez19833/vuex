import shop from '@/api/shop'

export default {
  namespaced: true,
  state: {
    items:[],
    checkoutStatus: null
  },

  getters: {
    cartProducts(state, getters, rootState, rootGetters) {
      return state.items.map(cartItem => {
        const product = rootState.products.items.find(product => product.id === cartItem.id)
        return {
          id: product.id,
          title: product.title,
          price: product.price,
          quantity: cartItem.quantity,
        }
      })
    },

    cartTotal(state, getters) {
      if (state.items.length === 0 ) {
        return 0;
      }
      return getters.cartProducts.reduce((total, item) => total + (item.quantity * item.price || 0), 0);
    }
  },

  mutations: {
    pushProductToCart(state, productId) {
      state.items.push({id: productId, quantity: 1})
    },

    pullProductFromCart(state, cartItem) {
      state.items = state.items.filter(item => item.id !== cartItem.id);
    },

    decrementItemQuantity(state, cartItem) {
      cartItem.quantity--;
    },

    incrementItemQuantity(state, cartItem) {
      cartItem.quantity++;
    },

    setCheckoutStatus(state, status) {
      state.checkoutStatus = status
    },

    emptyCart(state) {
      state.items = []
    }
  },

  actions: {
    fetchProducts({commit}) { // context object has same set of methods/properties as state object
      console.log('product/cart fetch')
    },

    checkout({state, commit}) {
      shop.buyProducts(state.items,
        () => {
          commit('emptyCart', state.items)
          commit('setCheckoutStatus', 'success')
        },
        () => {
          commit('setCheckoutStatus', 'fail')
        }
      )
    },

    // addProductToCart(context, product) { // arguements can be destructed like so:

    addProductToCart({state, getters, commit, rootState, rootGetters}, product) { // product is payload
      if (rootGetters['products/productIsInStock'](product)) {
        const cartItem = state.items.find(item => item.id === product.id)
        if (!cartItem) {
          commit('pushProductToCart', product.id)
        } else {
          commit('incrementItemQuantity', cartItem)
        }
        commit('products/decrementProductInventory', product, {root: true})
      } else {
        // show out of stock message
      }
    },

    removeProductFromCart({state, getters, commit, rootState}, cartItem) {
      let cartItem1 = state.items.find(item => item.id === cartItem.id)
      let product = rootState.products.items.find(item => item.id === cartItem.id)

      if (cartItem1) {
        if (cartItem.quantity === 1) {
          commit('pullProductFromCart', cartItem1)
        } else {
          commit('decrementItemQuantity', cartItem1)
        }
        commit('products/incrementProductInventory', product, {root: true})
      }
    }
  }
}
