import Vuex from 'vuex'
import Vue from 'vue'
import actions from './actions'
import cart from './modules/cart'
import products from './modules/products'

Vue.use(Vuex)

export  default new Vuex.Store({

  // state.state.modulename
  // getters/mutations/setters still under global namespace
  modules: {
    cart,
    products
  },

  state: {  // comparable to data in components, NEVER update state directly, always use a mutation
  },

  getters: { // comparable to computed in components
  },

  // decide when mutations to fire.
  // actions have been split into diff file
  actions,

  mutations: { // new, responsible for setting/updating state
  }

});
